// Leaflet.MapkitMutant - use (Apple's) MapkitJS basemaps in Leaflet.
// See https://gitlab.com/IvanSanchez/Leaflet.MapkitMutant

L.MapkitMutant = L.Layer.extend({
	options: {
		minZoom: 4,
		maxZoom: 19,

		// 🍂option type: String = 'standard'
		// mapkit's map type. Valid values are strings 'standard' (default),
		// 'satellite' or 'hybrid'.
		type: "standard",

		// 🍂option authorizationCallback: Function
		// An autorization callback function, as described
		// in [Apple's mapkitJS documentation](https://developer.apple.com/documentation/mapkitjs/mapkit/2974045-init)
		authorizationCallback: function () {},

		// 🍂option language: String = undefined
		// A language code, as described in
		// [Apple's mapkitJS documentation](https://developer.apple.com/documentation/mapkitjs/mapkit/2974045-init).
		// By default Mapkit will use the locale setting from the web browser.

		// 🍂option opacity: Number = 1.0
		// The opacity of the MapkitMutant
		opacity: 1,

		// 🍂option debugRectangle: Boolean = false
		// Whether to add a rectangle with the bounds of the mutant to the map.
		// Only meant for debugging, most useful at low zoom levels.
		debugRectangle: false,
	},

	initialize: function (options) {
		L.Util.setOptions(this, options);

		/// TODO: Add a this._mapkitPromise, just like GoogleMutant

		if (!L._mapkitInit) {
			L._mapkitInit = true;
			mapkit.init({
				authorizationCallback: this.options.authorizationCallback,
				language: this.options.language,
			});
		}
	},

	onAdd: function (map) {
		this._map = map;

		this._initMutantContainer();

		this._initMutant();

		map.on("move zoom moveend zoomend", this._update, this);
		map.on("resize", this._resize, this);
		map.on("fullscreenchange resize baselayerchange", this._delayedUpdate, this);
		this._resize();
	},

	beforeAdd: function (map) {
		map._addZoomLimit(this);
	},

	onRemove: function (map) {
		this.setOpacity(0);
		map._container.removeChild(this._mutantContainer);
		this._mutantContainer = undefined;
		map.off("move zoom moveend zoomend", this._update, this);
		map.off("resize", this._resize, this);
		map.off(
			"fullscreenchange resize baselayerchange",
			this._delayedUpdate,
			this
		);
		this._mutant.removeEventListener(
			"region-change-end",
			this._onRegionChangeEnd,
			this
		);
		if (this._canvasOverlay) {
			this._canvasOverlay.remove();
			this._canvasOverlay = undefined;
		}
		if (this._mutantCanvas) {
			this._mutantCanvas = undefined;
		}
		L.DomUtil.remove(this._mutantStyles);
	},

	// Create the HTMLElement for the mutant map, and add it as a children
	// of the Leaflet Map container
	_initMutantContainer: function () {
		if (!this._map._mutantContainer) {
			this._mutantContainer = L.DomUtil.create(
				"div",
				"leaflet-mapkit-mutant leaflet-top leaflet-left"
			);
			this._mutantContainer.id =
				"_MutantContainer_" + L.Util.stamp(this._mutantContainer);
			this._mutantContainer.style.zIndex = "401"; //leaflet map pane at 400, controls at 401
			this._mutantContainer.style.pointerEvents = "none";
			this._map._mutantContainer = this._mutantContainer;
		} else {
			this._mutantContainer = this._map._mutantContainer;
		}
		this._map.getContainer().appendChild(this._mutantContainer);
		this.setElementSize(this._mutantContainer, this._map.getSize());
	},

	// Create the mutant map inside the mutant container
	_initMutant: function () {
		if (!this._mutantContainer) return;

		var mapType = mapkit.Map.MapTypes.Standard;
		if (this.options.type === "hybrid") {
			mapType = mapkit.Map.MapTypes.Hybrid;
		} else if (this.options.type === "satellite") {
			mapType = mapkit.Map.MapTypes.Satellite;
		} else if (this.options.type === "muted") {
			mapType = mapkit.Map.MapTypes.MutedStandard;
		}

		var map;
		if (!this._map._mutant) {
			map = new mapkit.Map(this._mutantContainer, {
				visibleMapRect: this._leafletBoundsToMapkitRect(),
				showsUserLocation: false,
				showsUserLocationControl: false,

				// WTF, apple devs? other options are boolean but this is a
				// `mapkit.FeatureVisibility`. F*ck consistency, amirite?!
				showsCompass: "hidden",

				showsZoomControl: false,
				showsUserLocationControl: false,
				showsScale: false,
				showsMapTypeControl: false,
				mapType: mapType,
			});
			this._map._mutant = map;
		} else {
			map = this._map._mutant;
			map.mapType = mapType;
		}

		this._mutant = map;

		L.Util.requestAnimFrame(function extractMutantCanvas() {
			if (!this._mutantCanvas) {
				this._onRegionChangeEnd();
				L.Util.requestAnimFrame(extractMutantCanvas, this);
			}
		}, this);

		map.addEventListener("region-change-end", this._onRegionChangeEnd, this);

		this._initMapkitAttribution(mapType);
		this._initMutantStyles(mapType);
		this.setOpacity(1);

		// 🍂event spawned
		// Fired when the mutant has been created.
		this.fire("spawned", { mapObject: map });

		// Call _onRegionChangeEnd once, so that it can fetch the mutant's canvas and
		// create the L.ImageOverlay
		L.Util.requestAnimFrame(this._onRegionChangeEnd, this);
		L.Util.requestAnimFrame(this._update, this);
	},

	// Fetches the map's current *projected* (EPSG:3857) bounds, and returns
	// an instance of mapkit.MapRect
	_leafletBoundsToMapkitRect: function () {
		var bounds = this._map.getPixelBounds();
		var scale = this._map.options.crs.scale(this._map.getZoom());
		var nw = bounds.getTopLeft().divideBy(scale);
		var se = bounds.getBottomRight().divideBy(scale);

		// Map those bounds into a [[0,0]..[1,1]] range
		var projectedBounds = L.bounds([nw, se]);

		var projectedCenter = projectedBounds.getCenter();
		var projectedSize = projectedBounds.getSize();

		var result = new mapkit.MapRect(
			projectedCenter.x - projectedSize.x / 2,
			projectedCenter.y - projectedSize.y / 2,
			projectedSize.x,
			projectedSize.y
		);
		return result;
	},

	// Given an instance of mapkit.MapRect, returns an instance of L.LatLngBounds
	// This depends on the current map center, as to shift the bounds on
	// multiples of 360 in order to prevent artifacts when crossing the
	// antimeridian.
	_mapkitRectToLeafletBounds: function (rect) {
		// Ask MapkitJS to provide the lat-lng coords of the rect's corners
		var nw = new mapkit.MapPoint(rect.minX(), rect.maxY()).toCoordinate();
		var se = new mapkit.MapPoint(rect.maxX(), rect.minY()).toCoordinate();

		var lw = nw.longitude + Math.floor(rect.minX()) * 360;
		var le = se.longitude + Math.floor(rect.maxX()) * 360;

		var centerLng = this._map.getCenter().lng;

		// Shift the bounding box on the easting axis so it contains the map center
		if (centerLng < lw) {
			// Shift the whole thing to the west
			var offset = Math.floor((centerLng - lw) / 360) * 360;
			lw += offset;
			le += offset;
		} else if (centerLng > le) {
			// Shift the whole thing to the east
			var offset = Math.ceil((centerLng - le) / 360) * 360;
			lw += offset;
			le += offset;
		}

		return L.latLngBounds([
			L.latLng(nw.latitude, lw),
			L.latLng(se.latitude, le),
		]);
	},

	_update: function () {
		if (this._map && this._mutant) {
			clearTimeout(this._timeout);
			this._mutant.setVisibleMapRectAnimated(
				this._leafletBoundsToMapkitRect(),
				false
			);
			this._timeout = setTimeout(
				function () {
					// Force Mapkit rerender to solve map desynchronization issues
					this._mutant._impl.updatedLabelRegion();
				}.bind(this),
				100
			);

			//To allow fallback map to be shown for small zoom levels
			this._checkOpacity();
			this._map.off("zoomend", this._checkOpacity);
			this._map.on("zoomend", this._checkOpacity, this);
		}
	},

	/**
	 * We want to hide the apple map to show the underlying baseLayer for small zooms that mapkit does not accept.
	 * @private
	 */
	_checkOpacity: function () {
		if (this._map == null) {
			return;
		}

		if (this._map.getZoom() <= this.options.minZoom) {
			this.setOpacity(0);
		} else {
			this.setOpacity(1);
		}
	},

	_resize: function () {
		var size = this._map.getSize();
		if (
			this._mutantContainer.style.width === size.x &&
			this._mutantContainer.style.height === size.y
		)
			return;
		this.setElementSize(this._mutantContainer, size);
		if (!this._mutant) return;
	},

	_onRegionChangeEnd: function () {
		if (!this._mutantCanvas) {
			if (!this._map._mutantCanvas) {
				this._mutantCanvas = this._mutantContainer.querySelector(
					"canvas.syrup-canvas"
				);
				this._map._mutantCanvas = this._mutantCanvas;
			} else {
				this._mutantCanvas = this._map._mutantCanvas;
			}
		}

		if (this._map && this._mutantCanvas) {
			// Despite the event name and this method's name, fetch the mutant's
			// visible MapRect, not the mutant's region. It uses projected
			// coordinates (i.e. scaled EPSG:3957 coordinates). This prevents
			// latitude shift artifacts.
			var bounds = this._mapkitRectToLeafletBounds(
				this._mutant.visibleMapRect
			);

			// The mutant will take one frame to re-stitch its tiles, so
			// repositioning the mutant's overlay has to take place one frame
			// after the 'region-change-end' event, in order to avoid graphical
			// glitching.
			L.Util.requestAnimFrame(function () {
				if (!this._canvasOverlay) {
					if (!this._map._canvasOverlay) {
						this._canvasOverlay = L.imageOverlay(null, bounds);
						this._canvasOverlay.getEvents = function () {
							var events = {
								viewreset: this._reset,
							};
							if (this._zoomAnimated) {
								events.zoomanim = this._animateZoom;
							}
							return events;
						};

						// Hack the ImageOverlay's _image property so that it doesn't
						// create a HTMLImageElement
						var img = (this._canvasOverlay._image = L.DomUtil.create(
							"div"
						));

						L.DomUtil.addClass(img, "leaflet-image-layer");
						L.DomUtil.addClass(img, "leaflet-zoom-animated");

						// Move the mutant's canvas out of its container, and into
						// the L.ImageOverlay's _image
						this._mutantCanvas.parentElement.removeChild(
							this._mutantCanvas
						);
						img.appendChild(this._mutantCanvas);
						this._map._canvasOverlay = this._canvasOverlay;
					} else {
						this._canvasOverlay = this._map._canvasOverlay;
					}

					this._canvasOverlay.addTo(this._map);
					this._updateOpacity();
					this._mutantCanvas.style.width = "100%";
					this._mutantCanvas.style.height = "100%";
					this._mutantCanvas.style.position = "absolute";
					this._mutantCanvas.style.top = 0;
					this._mutantCanvas.style.left = 0;
				} else {
					this._canvasOverlay.setBounds(bounds);
				}

				if (this.options.debugRectangle) {
					if (!this.rectangle) {
						this.rectangle = L.rectangle(bounds, {
							fill: false,
						}).addTo(this._map);
					} else {
						this.rectangle.setBounds(bounds);
					}
				}
			}, this);
		}
	},

	// 🍂method setOpacity(opacity: Number): this
	// Sets the opacity of the MapkitMutant.
	setOpacity: function (opacity) {
		this.options.opacity = opacity;
		this._updateOpacity();
		return this;
	},

	/**
	 * We hide/show all the parts that show it's a mutant mapkit
	 * @private
	 */
	_updateOpacity: function () {
		//Mutant canvas to draw
		if (this._mutantCanvas) {
			L.DomUtil.setOpacity(this._mutantCanvas, this.options.opacity);
		}

		//Hide Mutant content (contains mapkit controls AKA legal and logo)
		if (this._mutantContainer) {
			L.DomUtil.setOpacity(this._mutantContainer, this.options.opacity);
		}

		//Leaflet native attribution control
		if (this._leafletNativeAttribution) {
			//We give margin-bottom to the leaflet bottom controls.
			var leafletControlCorners = this._map._controlCorners;
			var bottomRight = leafletControlCorners.bottomright;
			var bottomLeft = leafletControlCorners.bottomleft;
			if (this.options.opacity > 0) {
				this._leafletNativeAttribution.style.display = "none";
				bottomLeft.style.marginBottom = "16px";
				bottomRight.style.marginBottom = "16px";
			} else {
				this._leafletNativeAttribution.style.display = "block";
				bottomLeft.style.marginBottom = "0px";
				bottomRight.style.marginBottom = "0px";
			}
		}
	},

	_initMapkitAttribution: function (mapType) {
		this._leafletNativeAttribution = document.querySelector(
			".leaflet-control-attribution"
		);
		if (
			this._leafletNativeAttribution &&
			this._map.getZoom() <= this.options.minZoom
		) {
			this._leafletNativeAttribution.style.display = "none";
		}
	},

	_initMutantStyles: function (mapType) {
		this._mutantStyles = document.createElement("style");
		this._mutantStyles.textContent =
			"div.leaflet-mapkit-mutant.leaflet-top.leaflet-left[data-map-printing-background]{z-index:399!important;}.mk-logo-control img.mk-logo.mk-logo-wordmark{margin: 0 10px 7px 10px;z-index:400;}.mk-legal-controls{margin-right:0;margin-bottom:0;display:inline-block;vertical-align:top;margin-bottom:2px;pointer-events:auto;overflow:hidden;position:static;left:0;bottom:0}.mk-legal-controls :focus{outline:0}.mk-legal-controls .mk-legal{padding:4px 5px}";
		if (mapType === mapkit.Map.MapTypes.Standard) {
			this._mutantStyles.textContent +=
				".leaflet-container{background-color:#f9f5ed}";
		} else {
			this._mutantStyles.textContent +=
				".leaflet-container{background-color:#404040}";
		}
		document.querySelector("head").appendChild(this._mutantStyles);
	},

	_delayedUpdate: function (ev) {
		if (this._map) {
			var size = this._map.getSize();
			var countUpdates = 0;
			setTimeout(
				function delayedUpdate() {
					if (this._map) {
						var newSize = this._map.getSize();
						if (size.x === newSize.x && size.y === newSize.y) {
							this._update(ev);
							countUpdates++;
							if (countUpdates < 10) {
								setTimeout(
									delayedUpdate.bind(this),
									countUpdates * 40
								);
							}
						} else {
							countUpdates = 0;
							size = newSize;
							setTimeout(delayedUpdate.bind(this), 40);
						}
					}
				}.bind(this),
				40
			);
		}
	},

	setElementSize: function (e, size) {
		e.style.width = size.x + "px";
		e.style.height = size.y + "px";
	},
});

L.mapkitMutant = function mapkitMutant(options) {
	return new L.MapkitMutant(options);
};
